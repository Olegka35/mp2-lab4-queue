#include <stdlib.h>
#include <time.h>
#include "TJobStream.h"

TJobStream::TJobStream(int _q1, int _q2) : ID(0), q1(_q1), q2(_q2)
{
	srand(time(NULL));
}

int TJobStream::CreateJob()
{
	int result = 0;
	if (rand() % 100 < q1)
		result = ++ID;
	return result;
}

bool TJobStream::EndJob() const
{
	bool result = false;
	if (rand() % 100 < q2)
		result = true;
	return result;
}


int TJobStream::GetID() const
{
	return ID;
}
